
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import 'vuetify/dist/vuetify.min.css'
import Vuetify from 'vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Router from 'vue-router'
import VueProgressBar from 'vue-progressbar'
import router from './router/router.js'

Vue.component('maincomponent', require('./components/MainComponent.vue'));
Vue.component('sidebar', require('./components/Sidebar.vue'));

Vue.use(Vuetify,VueAxios,axios,Router)
Vue.use(VueProgressBar, {
    color: 'white',
    failedColor: 'red',
    thickness: '3px'
  })


const app = new Vue({
    el: '#app',
    // router,
    // created(){
    //     this.$router.push('materiel');
    // }
});
